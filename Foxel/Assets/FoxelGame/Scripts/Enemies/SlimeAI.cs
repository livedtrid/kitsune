﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.CorgiEngine;
using MoreMountains.Tools;
using System;

public class SlimeAI : AIWalk
{

    #region FSM

    public enum SLIME_STATES { IDLE, ALERT, ATTACK }


    private SLIME_STATES slime_states;

    private SLIME_STATES _SLIME_STATES
    {
        get { return slime_states; }
        set
        {
            OnStateExit(slime_states);
            slime_states = value;
            OnStateEnter(value);
        }
    }

    private void OnStateExit(SLIME_STATES currentState)
    {

        switch (currentState)
        {
            case SLIME_STATES.IDLE:
                OnIdleExit();
                break;
            case SLIME_STATES.ALERT:
                OnAlertExit();
                break;
            case SLIME_STATES.ATTACK:
                OnAttackExit();
                break;
            default:
                break;
        }
    }

    private void OnStateEnter(SLIME_STATES currentState)
    {

        switch (currentState)
        {
            case SLIME_STATES.IDLE:
                OnIdleEnter();
                break;
            case SLIME_STATES.ALERT:
                OnAlertEnter();
                break;
            case SLIME_STATES.ATTACK:
                OnAttackEnter();
                break;
            default:
                break;

        }


    }

    private void OnIdleExit()
    {
        Debug.Log("Idle exit");
    }

    private void OnAlertExit()
    {
        Debug.Log("Alert exit");
    }

    private void OnAttackExit()
    {
        Debug.Log("Attack exit");
    }

    private void OnIdleEnter()
    {
        Debug.Log("Idle enter");
       animator.SetTrigger("Idle");
    }

    private void OnAlertEnter()
    {
        Debug.Log("Alert enter");
        animator.SetTrigger("Idle");
    }

    private void OnAttackEnter()
    {
        Debug.Log("Attack enter");
        animator.SetTrigger("Attack");
    }

    #endregion

    public Animator animator;

}