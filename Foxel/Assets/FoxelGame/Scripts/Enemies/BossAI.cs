﻿using System.Collections;
using UnityEngine;
using MoreMountains.CorgiEngine;

[RequireComponent(typeof(Animator))]
public class BossAI : MonoBehaviour
{

    #region FSM

    public enum BOSS_STATES { IDLE, SPLASH, STOMP, CLAP, DAMAGE }


    private BOSS_STATES boss_states;

    private BOSS_STATES _BOSS_STATES
    {
        get { return boss_states; }
        set
        {
            OnStateExit(boss_states);
            boss_states = value;
            OnStateEnter(value);
        }
    }

    private void OnStateExit(BOSS_STATES currentState)
    {
        Debug.Log("EXIT " + currentState.ToString());

        switch (currentState)
        {
            case BOSS_STATES.IDLE:
                OnIdleExit();
                break;
            case BOSS_STATES.SPLASH:
                OnSplashExit();
                break;
            case BOSS_STATES.STOMP:
                OnStompExit();
                break;
            case BOSS_STATES.CLAP:
                OnClapExit();
                break;
            case BOSS_STATES.DAMAGE:
                OnDamageExit();
                break;
            default:
                break;
        }
    }

    private void OnStateEnter(BOSS_STATES currentState)
    {
        Debug.Log("ENTER " + currentState.ToString());
        switch (currentState)
        {
            case BOSS_STATES.IDLE:
                OnIdleEnter();
                break;
            case BOSS_STATES.SPLASH:
                OnSplashEnter();
                break;
            case BOSS_STATES.STOMP:
                OnStompEnter();
                break;
            case BOSS_STATES.CLAP:
                OnClapEnter();
                break;
            case BOSS_STATES.DAMAGE:
                OnDamageEnter();
                break;
            default:
                break;
        }
    }

    public void SetIdle() { _BOSS_STATES = BOSS_STATES.IDLE; }
    public void SetSplashSttack() { _BOSS_STATES = BOSS_STATES.SPLASH; }
    public void SetStompAttack() { _BOSS_STATES = BOSS_STATES.STOMP; }
    public void SetClapAttack() { _BOSS_STATES = BOSS_STATES.CLAP; }
    public void SetDamage() { _BOSS_STATES = BOSS_STATES.DAMAGE; }

    #endregion FSM

    #region PRIVATE_MEMBERS

    [Range(0, 10)]
    [SerializeField]
    private int stamina = 10;

    [Range(0, 3)]
    [SerializeField]
    private int bossLives = 3;

    private Health health;
    private AutoRespawn autoRespawn;
    private bool isPlayerOnCenter = true;

    public ScriptedFallingPlatform[] fallingPlatforms;


    private Animator animator;

    #endregion

    #region PUBLIC_MEMBERS
    public GameObject criticalPoint, rightHand, leftHand;
    private bool isAnimationFinished;

    #endregion

    #region MONOBEHAVIOUR
    // Use this for initialization
    void Start()
    {
        health = criticalPoint.GetComponent<Health>();
        autoRespawn = criticalPoint.GetComponent<AutoRespawn>();
        health.OnDeath += OnDeath;

        animator = GetComponent<Animator>();
        if (!animator)
        {
            Debug.LogError("Animator not found");
        }

        StartCoroutine(UpdateStateMachine());
    }

    public void UpdateAnimations()
    {
        isAnimationFinished = true;

        StartCoroutine(UpdateStateMachine());
    }
    
    public void IsPlayerOnCenter(bool value)
    {
        isPlayerOnCenter = value;
    }

    //Called by animation Stomp
    public void DestroyPlatforms(int platformIndex)
    {
        if (bossLives == 2)
        {
            if (platformIndex < 2)
            {

                fallingPlatforms[platformIndex].FallImmediately();
            }
        }

        if (bossLives == 1)
        {
            if (platformIndex >= 2)
            {
                fallingPlatforms[platformIndex].FallImmediately();
            }
        }
    }

    private IEnumerator UpdateStateMachine()
    {
        isAnimationFinished = false;

        Debug.Log("UpdateStateMachine");
        //Get player position

        //Check stamina

        //Decide what to do

        if (stamina > 0)
        {

            if (isPlayerOnCenter)
            {
                int i = UnityEngine.Random.Range(0, 2);

                switch (i)
                {
                    case 0:
                        SetSplashSttack();
                        break;
                    case 1:
                        SetClapAttack();                      
                        break;
                    default:
                        SetClapAttack();
                        break;
                }

            }
            else
            {
                SetStompAttack();


            }


        }
        else
        {
            StartCoroutine(StartRecovery());
        }

        while (!isAnimationFinished)
        {

            yield return new WaitForEndOfFrame();
        }       

    }

    private IEnumerator StartRecovery()
    {
        SetIdle();

        while (stamina < 3)
        {
            stamina++;

            yield return new WaitForSeconds(2.0f);
        }        
    }

    // Update is called once per frame
    void Update()
    {

    }
    #endregion

    #region PRIVATE_METHODS


    private void OnDeath()
    {
        Debug.Log("Boss damage");

        SetDamage();
    }

    private void EnableHandsDamage(bool value)
    {
        rightHand.GetComponent<DamageOnTouch>().enabled = value;
        leftHand.GetComponent<DamageOnTouch>().enabled = value;

        if (value)
        {
            rightHand.layer = LayerMask.NameToLayer("Enemies");
            leftHand.layer = LayerMask.NameToLayer("Enemies");
        }
        else
        {
            rightHand.layer = LayerMask.NameToLayer("MovingPlatforms");
            leftHand.layer = LayerMask.NameToLayer("MovingPlatforms");
        }
    }

    private void OnIdleEnter()
    {
        EnableHandsDamage(false);
        animator.SetTrigger("idle");
        criticalPoint.SetActive(true);
    }

    private void OnSplashEnter()
    {
        animator.SetTrigger("splash");
        stamina--;
    }

    private void OnStompEnter()
    {
        animator.SetTrigger("stomp");
        stamina--;
    }

    private void OnClapEnter()
    {
        animator.SetTrigger("clap");
        stamina--;
    }

    private void OnDamageEnter()
    {
        animator.SetTrigger("hurt");

        bossLives--;
        Mathf.Clamp(bossLives, 0, 3);

        if (bossLives == 0)
        {
            Debug.Log("Call death animation");
            animator.SetTrigger("death");

            LevelManager levelManager = FindObjectOfType<LevelManager>();
            levelManager.SetNextLevel("03_Credits");
            levelManager.GotoNextLevel();
        }
        else
        {
            autoRespawn.Revive();
            stamina = 3;
        }
    }

    private void OnIdleExit()
    {
        EnableHandsDamage(true);
        criticalPoint.SetActive(false);

    }

    private void OnSplashExit()
    {
        //throw new NotImplementedException();
    }

    private void OnStompExit()
    {
        //throw new NotImplementedException();
    }

    private void OnClapExit()
    {
        // throw new NotImplementedException();
    }

    private void OnDamageExit()
    {

    }
    #endregion
}