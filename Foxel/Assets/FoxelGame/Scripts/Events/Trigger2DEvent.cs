﻿using UnityEngine.Events;
using UnityEngine;
using System;

[Serializable]
public class Trigger2DEvent : UnityEvent<Collider2D> {}
