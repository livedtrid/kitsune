﻿using UnityEngine;

public class MusicTrigger : MonoBehaviour {

    public Trigger2DEvent trigger2DEvent;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        trigger2DEvent.Invoke(collision);
    }
}
