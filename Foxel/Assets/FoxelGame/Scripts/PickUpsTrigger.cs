﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.CorgiEngine;
using UnityEngine.UI;

public class PickUpsTrigger : MonoBehaviour
{

    public enum PICKUP_TYPE { DASH, DOUBLE_JUMP, ORB, HEART }

    public PICKUP_TYPE pickupType = PICKUP_TYPE.HEART;


    //GameObject doubleJump;

    public Image powerUPicon, dashImage; // Poes a imagem no inspector
    public Text orbsQuant;
    public int orbsCount=0;

    GameObject player;

    public AudioClip OrbCollectSfx;


    // Use this for initialization
    void Start()
    {

        player = GameObject.FindGameObjectWithTag("Player");
        //doubleJump = GameObject.FindGameObjectWithTag("DoubleJumpUI");
        //doubleJump.GetComponent<Image>().enabled = false;

        // dashImage.enabled = false;
        //orbsQuant = gameObject.GetComponent<Text>();

        if (pickupType.Equals(PICKUP_TYPE.DASH) || pickupType.Equals(PICKUP_TYPE.DOUBLE_JUMP))
        {
            powerUPicon.enabled = false;
        }



    }



    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.transform.tag == "Player")
        {


            switch (pickupType)
            {
                case PICKUP_TYPE.DASH:
                    player.GetComponent<CharacterDash>().AbilityPermitted = true;
                    powerUPicon.enabled = true;
                    //powerUPicon.enabled = true;
                    break;
                case PICKUP_TYPE.DOUBLE_JUMP:
                    player.GetComponent<CharacterJump>().NumberOfJumps = 2;
                    powerUPicon.enabled = true;
                    //doubleJump.GetComponent<Image>().enabled = true;
                    //powerUPicon.enabled = true;
                    break;
                case PICKUP_TYPE.ORB:
                    PlayOrCollectSfx();
                    int i = ParseFast(orbsQuant.text);
                    i++;
                    orbsQuant.text = i.ToString();

                    TextMesh signText = GameObject.Find("OrbsCollected").GetComponent<TextMesh>();
                    signText.text = i.ToString();

                    if (i>6)
                    {
                        FinishLevel finishLevel = FindObjectOfType<FinishLevel>();
                        finishLevel.GetComponent<BoxCollider2D>().enabled = true;

                        GameObject portal = GameObject.Find("RetroPortalOrange");
                        portal.SetActive(false);
                    }
                    break;
                case PICKUP_TYPE.HEART:
                    break;
                default:
                    break;
            }
            

            Destroy(gameObject);

        }
    }

    protected virtual void PlayOrCollectSfx()
    {
        if (OrbCollectSfx != null) { SoundManager.Instance.PlaySound(OrbCollectSfx, transform.position); }
    }

    public static int ParseFast(string s)
    {
        int r = 0;
        for (var i = 0; i < s.Length; i++)
        {
            char letter = s[i];
            r = 10 * r;
            r = r + (int)char.GetNumericValue(letter);
        }
        return r;
    }


}


