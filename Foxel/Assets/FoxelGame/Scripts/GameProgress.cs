﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.CorgiEngine;

public class GameProgress : MonoBehaviour {

    public GameObject[] blueDoors, yellowDoors, orangeDoors;

    [SerializeField]
    private WEAPON_TYPE _weapon_type = WEAPON_TYPE.WHITE;

    public WEAPON_TYPE WeaponType
    {
        get { return _weapon_type; }
        set
        {
            ExitState(_weapon_type);
            _weapon_type = value;
            EnterState(_weapon_type);
        }
    }

    private void EnterState(WEAPON_TYPE weapon_type)
    {
        switch (weapon_type)
        {
            case WEAPON_TYPE.WHITE:
                
                break;
            case WEAPON_TYPE.BLUE:
                //Enable all blue doors
                StartCoroutine(EnableDoors(blueDoors));
                break;
            case WEAPON_TYPE.YELLOW:
                //Enable all yellow doors
                StartCoroutine(EnableDoors(yellowDoors));
                break;
            case WEAPON_TYPE.ORANGE:
                //Enable all orange doors
                StartCoroutine(EnableDoors(orangeDoors));
                break;
            default:
                break;
        }
    }

    private void ExitState(WEAPON_TYPE weapon_type)
    {
        switch (weapon_type)
        {
            case WEAPON_TYPE.WHITE:
                break;
            case WEAPON_TYPE.BLUE:
                break;
            case WEAPON_TYPE.YELLOW:
                break;
            case WEAPON_TYPE.ORANGE:
                break;
            default:
                break;
        }
    }

    public void SetCurrentWeapon(int weapon)
    {
        WeaponType = (WEAPON_TYPE)weapon;
    }

    private void Start()
    {
        blueDoors = GameObject.FindGameObjectsWithTag("BlueDoors");
        yellowDoors = GameObject.FindGameObjectsWithTag("YellowDoors");
        orangeDoors = GameObject.FindGameObjectsWithTag("OrangeDoors");


        StartCoroutine(DisableDoors(blueDoors));
        StartCoroutine(DisableDoors(yellowDoors));
        StartCoroutine(DisableDoors(orangeDoors));
    }


    private IEnumerator EnableDoors(GameObject[] doorsToEnable)
    {
        for (int i = 0; i < doorsToEnable.Length; i++)
        {
            doorsToEnable[i].GetComponent<Health>().Invulnerable = false;
            yield return null;
        }
    }

    private IEnumerator DisableDoors(GameObject[] doorsToEnable)
    {
        for (int i = 0; i < doorsToEnable.Length; i++)
        {
            doorsToEnable[i].GetComponent<Health>().Invulnerable = true;
            yield return null;
        }
    }


}
